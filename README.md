# API REST

## Some usefull commands

- **Start the server :** `php -S localhost:8000 -t public`
- **Create database table :** `php artisan make:migration create_[here the name of the table]_table`
- **Reset the database :** `php artisan migrate:refresh`
- **Fill the database with some data :** `php artisan db:seed`
- **Launch the test :** `./vendor/bin/phpunit`
- For this command, you need to fille the database before.

## Routes

- **Signification of 🔐 :** You need to be login, then take the bearer token, and put it into the header's files "Authorization". You need to specify before "bearer" and after a space, put the token into the files. 

### Users

- **ShowAllUsers :** `GET http://localhost:8000/api/users`
- **ShowOneUser :** `GET http://localhost:8000/api/users/{id}`
- **Profile :** `GET http://localhost:8000/api/users/profile` 🔐
- **CountUsers :** `GET http://localhost:8000/api/users/count`
- **Register :** `POST http://localhost:8000/api/auth/register`
- Fill the body fields "pseudo" "email" "password" "nationality" (2 Uppercase letters)
- **Login :** `POST http://localhost:8000/api/auth/login` 
- Fill the body fields "email" "password"
- **VerifyPassword :** `POST http://localhost:8000/api/users/verify`
- **UploadImage** `POST http://localhost:8000/api/users/upload` 🔐
- Fill the body "image" with an existing file in your pc
- **Update user :** `PUT http://localhost:8000/api/users/{id}` 🔐
- Fill the body fields (urlencoded) "pseudo" "email" "password" "isAdmin" "isMember" "nation"
- **Delete user :** `DELETE http://localhost:8000/api/users/{id}` 🔐
 
### Articles


- **ShowAllArticles :** `GET http://localhost:8000/api/articles`
- **ShowOneUser :** `GET http://localhost:8000/api/articles/{id}`
- **ShowAllCommentsOfArticle :** `GET http://localhost:8000/api/articles/comments/{id}`
- **CountArticles :** `GET http://localhost:8000/api/articles/count`
- **CountCommentsPerArticle :** `GET http://localhost:8000/api/articles/comments/{id}/count`
- **Create :** `POST - http://localhost:8000/api/articles` 🔐
- Fill the body fields "id_user" "title" "summary" "body" (You can put an image into the fields "image")
- **UpdatePicture :** `POST - http://localhost:8000/api/articles/{id}/picture` 🔐
- Fill the body "image" with an existing file in your pc
- **Update :** `PUT - http://localhost:8000/api/articles/{id}` 🔐
- Fill the body fields "id_user" "title" "summary" "body"
- **Delete :** `DELETE - http://localhost:8000/api/articles/{id}` 🔐

### Comments

- **ShowAllComments :** `GET - http://localhost:8000/api/comments`
- **ShowOneComment :** `GET - http://localhost:8000/api/comments/{id}`
- **CountComments :** `GET - http://localhost:8000/api/comments/count`
- **Create :** `POST - http://localhost:8000/api/comments` 🔐
- Fill the body fields "id_article" "body"
- **Update :** `PUT - http://localhost:8000/api/comments/{id}` 🔐
- Fill the body fields "id_article" "body"
- **Delete :** `DELETE - http://localhost:8000/api/comments/{id}` 🔐

### Galerie

- **ShowAllImages :** `GET - http://localhost:8000/api/galerie`
- **ShowOneImage :** `GET - http://localhost:8000/api/galerie/{id}`
- **UploadImage :** `POST - http://localhost:8000/api/galerie`
- Fill the body fields "image" (with a picture)
- **Delete :** `DELETE - http://localhost:8000/api/galerie/{id}`