<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Article;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ArticleController extends Controller
{

    public function __construct()
    {
        
        $this->middleware('auth', ['except' => ['showAllArticles', 'showOneArticle', 'showAllCommentsOfOneArticle']]);

    }

    public function showAllArticles(Request $request)
    {
        
        try{
            
            $limit = $request->input('limit');
            $offset = $request->input('offset');

            $articles = DB::table('articles')->where('deleted_at', NULL)->orderBy('created_at', 'ASC')->offset($offset)->limit($limit)->get();

            return with(['articles' => $articles]);

        } catch (\Exception $e) {

            return response()->json(['message' => 'Method Failed!'], 409);

        }

    }

    public function showOneArticle($id)
    {

        try{

            $article = Article::findOrFail($id);

            return response()->json(['article' => $article], 200);

        } catch (\Exception $e) {

            return response()->json(['message' => 'Article not found!'], 404);

        }

    }

    public function showAllCommentsOfOneArticle($id, Request $request)
    {

        try {
            $limit = $request->input('limit');
            $offset = $request->input('offset');

            $comments = DB::table('comments')->where('id_article', $id)->orderBy('created_at', 'ASC')->offset($offset)->limit($limit)->get();

            return with(['comments' => $comments]);
        } catch (\Exception $e) {

            return response()->json(['message' => 'Method Failed!'], 409);

        }

    }

    public function create(Request $request)
    {

        try {
            $this->validate($request, [
                'title' => array(
                    'required'
                ),
                'summary' => array(
                    'required'
                ),
                'body' => array(
                    'required'
                )
            ]);
        } catch (\Exception $e) {

            return response()->json(['message' => 'invalid field title, summary or body'], 409);

        }

        try {
            $isMember = User::findOrFail(Auth::id());

            if (! $isMember->isMember == 1) {
                return response()->json(['message' => 'You not have the right to do this'], 412);
            }
        } catch (\Exception $e) {
            return response()->json(['message' => 'User not found!'], 404);
        }

        try{

            $article = new Article;
            $article->id = Str::uuid();
            $article->id_user = Auth::id();
            $article->title = $request->input('title');
            $article->summary = $request->input('summary');
            $article->body = $request->input('body');

            $picture = $request->file('picture');

            if($picture != NULL) {

                $request = new Request();

                $request->replace(['folder' => 'article']);
                $request->files->set('image', $picture);

                $url = json_decode($this->uploadImage($request, NULL)->getContent(), true);

                $article->picture = $url['data']['image'];
            }else{

                $article->picture = 'default_article.png';

            }

            $article->save();

            return response()->json(['article' => $article, 'message' => 'CREATED'], 201);

        } catch (\Exception $e) {

            return response()->json(['message' => 'Article Creation Failed! File to big or bad argument!'], 409);

        }

    }

    public function update($id, Request $request)
    {

        try{

            $article = Article::findOrFail($id);

            if($article->id_user != Auth::id()){

                return response()->json(['message' => 'You\'re not the user'], 412);

            }

            $article->update($request->all());

            return response()->json($article, 200);

        } catch (\Exception $e) {

            return response()->json(['message' => 'Article not found!'], 404);

        }

    }

    public function updatePicture($id, Request $request)
    {
        try {
            $article = Article::findOrFail($id);

            if ($article->id_user == Auth::id()) {

                $url = json_decode($this->uploadImage($request, $id)->getContent(), true);

                DB::table('articles')->where('id', $id)->update(['picture' => $url['data']['image']]);

            }else{
                return response()->json(['message' => 'You not have the right to do this'], 412);
            }

        } catch (\Exception $e) {
            return response()->json(['message' => 'Method Failed! File is to big'], 409);
        }
    }

    public function delete($id)
    {

        try{

            $isAdmin = User::findOrFail(Auth::id());
            $article = Article::findOrFail($id);

            if($isAdmin->isAdmin == 1 || $article->id_user == Auth::id()){

                Article::findOrFail($id)->delete();
                return response('Deleted Successfully', 200);

            }

            return response()->json(['message' => 'You not have the right to do this'], 412);

        } catch (\Exception $e) {

            return response()->json(['message' => 'Article not found!'], 404);

        }

    }

}