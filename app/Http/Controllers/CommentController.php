<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Comment;
use App\User;
use App\Article;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class CommentController extends Controller
{

    public function __construct()
    {
        
        $this->middleware('auth', ['except' => ['showAllComments', 'showOneComment']]);

    }

    public function showAllComments(Request $request)
    {

        try{

            $limit = $request->input('limit');
            $offset = $request->input('offset');

            $comments = DB::table('comments')->orderBy('created_at', 'ASC')->offset($offset)->limit($limit)->get();

            return with(['comments' => $comments]);

        } catch (\Exception $e){

            return response()->json(['message' => 'Method Failed!'], 409);

        }

    }

    public function showOneComment($id)
    {

        try{

            $comment = Comment::findOrFail($id);

            return response()->json(['comment' => $comment], 200);

        } catch (\Exception $e) {

            return response()->json(['message' => 'Comment not found!'], 404);

        }

    }

    public function create(Request $request)
    {

        try {
            $this->validate($request, [
                'id_article' => array(
                    'required'
                ),
                'body' => array(
                    'required'
                )
            ]);
        } catch (\Exception $e) {

            return response()->json(['message' => 'Invalid field body or article reference'], 409);

        }

        try {
            $isMember = User::findOrFail(Auth::id());

            if (! $isMember->isMember == 1) {
                return response()->json(['message' => 'You not have the right to do this'], 412);
            }
        } catch (\Exception $e) {
            return response()->json(['message' => 'User not found!'], 404);
        }

        try{

            $comment = new Comment;
            $comment->id = Str::uuid();
            $comment->id_user = Auth::id();
            $comment->id_article = $request->input('id_article');
            $comment->body = $request->input('body');

            $comment->save();

            return response()->json(['comment' => $comment, 'message' => 'CREATED'], 201);

        } catch (\Exception $e) {

            return response()->json(['message' => 'Comment Creation Failed!'], 409);

        }

    }

    public function update($id, Request $request)
    {

        try{

            $comment = Comment::findOrFail($id);

            if($comment->id_user != Auth::id()){

                return response()->json(['message' => 'You\'re not the user'], 412);

            }

            $comment->update($request->all());

            return response()->json($comment, 200);

        } catch (\Exception $e){

            return response()->json(['message' => 'Comment not found!'], 404);

        }

    }

    public function delete($id)
    {

        try{

            $isAdmin = User::findOrFail(Auth::id());
            $comment = Comment::findOrFail($id);

            if ($isAdmin->isAdmin == 1 || $comment->id_user == Auth::id()) {

                Comment::findOrFail($id)->delete();
                return response('Deleted Successfully', 200);

            }

            return response()->json(['message' => 'You not have the right to do this'], 412);

        } catch (\Exception $e) {

            return response()->json(['message' => 'Comment not found!'], 404);

        }

    }

}