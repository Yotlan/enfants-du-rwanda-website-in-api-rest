<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    
    public function __construct()
    {
        
        $this->middleware('auth', ['except' => ['showAllUsers', 'showOneUser']]);

    }

    public function profile()
    {
        return response()->json(['user' => Auth::user()], 200);
    }

    public function showAllUsers(Request $request)
    {

        //try {

            $limit = $request->input('limit');
            $offset = $request->input('offset');

            $users = DB::table('users')->where('deleted_at', NULL)->orderBy('created_at', 'ASC')->offset($offset)->limit($limit)->get();

            return with(['users' => $users]);

        /*} catch (\Exception $e) {

            return response()->json(['message' => 'Method Failed!'], 409);

        }*/

    }

    public function showOneUser($id)
    {
        try {

            $user = User::findOrFail($id);

            return response()->json(['user' => $user], 200);

        } catch (\Exception $e) {

            return response()->json(['message' => 'User not found!'], 404);

        }

    }

    public function update($id, Request $request)
    {

        try{

            $isAdmin = User::findOrFail(Auth::id());
            $user = User::findOrFail($id);

            if( ($request->input('isAdmin') != NULL || $request->input('isMember') != NULL) && User::findOrFail(Auth::id())->isAdmin != 1){
                return response()->json(['message' => 'You not have the right to do this'], 412);
            }

            if($isAdmin->isAdmin == 1 || $user->id == Auth::id()){

                if($request->input('password') != NULL){
                    $plainPassword = $request->input('password');
                    $password = app('hash')->make($plainPassword);

                    $request->replace(['password' => $password]);
                }

                $user->update($request->all());

                return response()->json($user, 200);

            }

            return response()->json(['message' => 'You not have the right to do this'], 412);

        } catch (\Exception $e) {

            return response()->json(['message' => 'User not found!'], 404);

        }
    }

    public function delete($id)
    {

        try {

            $isAdmin = User::findOrFail(Auth::id());
            $user = User::findOrFail($id);

            if($isAdmin->isAdmin == 1|| $user->id == Auth::id()){

                User::findOrFail($id)->delete();
                return response('Deleted Successfully', 200);

            }

            return response()->json(['message' => 'You not have the right to do this'], 412);

        } catch (\Exception $e) {

            return response()->json(['message' => 'User not found'], 404);

        }

    }

    public function uploadImageIntoUser(Request $request, $id){

        try{

            $user = User::findOrFail($id);

            if ($user->id == Auth::id()) {

                $url = json_decode($this->uploadImage($request, $id)->getContent(), true);

                $newRequest = new Request();

                $newRequest->replace(['picture' => $url['data']['image'] ]);

                return $this->update($id, $newRequest);

            }

            return response()->json(['message' => 'You not have the right to do this'], 412);

        }catch (\Exception $e){

            return response()->json(['message' => 'Method Failed! Maybe file is to big'], 409);

        }

    }

}