<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Str;

class AuthController extends Controller{

    /**
    * Store a new user.
    * 
    * @param Request $request 
    * @return Response
    */
    public function register(Request $request)
    {

        try {
            $this->validate($request, [
            'pseudo' => array(
                'required',
                'unique:users,pseudo',
                'regex:/^.{3,50}$/'),
            'email' => array(
                'required',
                'email',
                'unique:users,email'),
            'password' => array(
                'required',
                'regex:/^.{8,30}$/',
                'regex:/[A-Z]/',
                'regex:/[0-9]/',
                'regex:/[\#!^$()[\]{}?+*.\|]/'),
            'nationality' => array(
                'required',
                'alpha')
        ]);
        } catch (\Exception $e) {

            return response()->json(['message' => 'Field pseudo must contain between 3 and 50 characters, email must be valide, password must contain between 8 and 30 characters, 1  or more Uppercase letter, 1 or more number and 1 or more special charactere (\#!^$()[]{}?+*.|)'], 409);

        }

        try{

            $user = new User;
            $user->id = Str::uuid();
            $user->pseudo = $request->input('pseudo');
            $user->email = $request->input('email');
            $plainPassword = $request->input('password');
            $user->password = app('hash')->make($plainPassword);
            $user->nationality = $request->input('nationality');
            $user->picture = 'default_user.png';

            $user->save();

            return response()->json(['user' => $user, 'message' => 'CREATED'], 201);

        } catch (\Exception $e){

            return response()->json(['message' => 'User Registration Failed!'], 409);

        }

    }

    public function login(Request $request){

        try {
            $this->validate($request, [
                'email' => array(
                    'required',
                    'string'),
                'password' => array(
                    'required',
                    'string')
            ]);

            $email = $request->input('email');
            $password = $request->input('password');

            if (! $token = Auth::attempt(['email' => $email, 'password' => $password])) {
                return response()->json(['message' => 'Unauthorized'], 401);
            }
        
            return $this->respondWithToken($token);
        } catch (\Exception $e){

            return response()->json(['message' => 'Connexion Failed! Invalid email or password'], 409);

        }

    }

}