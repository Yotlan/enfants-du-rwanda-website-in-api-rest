<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Galerie;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class GalerieController extends Controller
{

    public function __construct()
    {
        
        $this->middleware('auth', ['except' => ['showAllImages', 'showOneImage']]);

    }

    public function showAllImages(){

        try {

            $galerie = DB::table('galerie')->where('deleted_at', NULL)->orderBy('created_at', 'ASC')->get();

            return with(['galerie' => $galerie]);

        } catch (\Exception $e) {

            return response()->json(['message' => 'Method Failed!'], 409);

        }

    }

    public function showOneImage($id){
        try {

            $image = Galerie::findOrFail($id);

            return response()->json(['image' => $image], 200);

        } catch (\Exception $e) {
            return response()->json(['message' => 'Image not found!'], 404);
        }
    }

    public function uploadImageIntoGalerie(Request $request){

        try {

            $user = User::findOrFail(Auth::id());

            if ($user->isAdmin == 1) {

                return $this->uploadImage($request, NULL);
            }else{
                return response()->json(['message' => 'You not have the right to do this'], 412);
            }

        }catch (\Exception $e){
            return response()->json(['message' => 'Method Failed! Maybe file is to big'], 409);
        }
    }

    //Delete image

    public function deleteImage($id){

        try {

            $user = User::findOrFail(Auth::id());

            if ($user->isAdmin != 1) {

                return response()->json(['message' => 'You not have the right to do this'], 412);
            }

            $file = Galerie::findOrFail($id)->url;

            unlink(storage_path('../public/' . $file));
            //unlink('D:/MAMP/htdocs/lesenfantsdurwanda_api-rest/public/' . $file);

            Galerie::findOrFail($id)->delete();

            return response('Deleted Successfully', 200);

        } catch(Exception $e) {

            return $this->responseRequestError('File not found', 404);

        }

    }

}