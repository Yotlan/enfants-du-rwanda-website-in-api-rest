<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Galerie;
use App\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class Controller extends BaseController
{

    public function __construct()
    {
        
        $this->middleware('auth', ['only' => ['uploadImage', 'verifyPassword']]);

    }

    //Token

    protected function respondWithToken($token)
    {
        return response()->json([
            'token' => $token,
            'token_type' => 'bearer',
            'expires_in' => Auth::factory()->getTTL()*60*24 //Duration of the token : 1 day
        ], 200);
    }

    //Upload image

    public function uploadImage(Request $request, $id)
    {

        try{

            $this->validate($request, [
                'folder' => array(
                    'required',
                    'alpha')
            ]);

            $folder = $request->input('folder');

            if(strcmp($folder, "user") !== 0 && strcmp($folder, "article") !== 0 && strcmp($folder, "galerie") !== 0){
                return response()->json(['message' => 'Incorrect folder name : choose between user, article and galerie'], 409);
            }

        } catch (\Exception $e) {

            return response()->json(['message' => 'Incorrect folder name : only in lowercase'], 409);

        }

        $response = null;
        $user = (object) ['image' => ""];

        if ($request->hasFile('image')) {

            if(!$request->file('image')->isValid()){
                return response()->json(['message' => 'Invalid image'], 409);
            }

            $original_filename = $request->file('image')->getClientOriginalName();

            if(filesize($original_filename > 1000000)){
                return $this->responseRequestError('File is to heavy');
            }

            $original_filename_arr = explode('.', $original_filename);
            $file_ext = end($original_filename_arr);

            if($folder == 'user'){

                $destination_path = storage_path('../public/upload/user/');
                //$destination_path = './upload/user/';

            }else if($folder == 'article'){

                $destination_path = storage_path('../public/upload/article/');
                //$destination_path = './upload/article/';

            }else if($folder == 'galerie'){

                $destination_path = storage_path('../public/upload/galerie/');
                //$destination_path = './upload/galerie/';

            }

            $image = time() . '.' . $file_ext;

            if ($request->file('image')->move($destination_path, $image)) {

                if ($folder == 'user') {

                    $user->image = '/upload/user/' . $image;

                }else if($folder == 'article'){

                    $user->image = 'upload/article/' . $image;

                }else if($folder == 'galerie'){

                    $user->image = 'upload/galerie/' . $image;

                    $galerie = new Galerie;
                    $galerie->id = Str::uuid();
                    $galerie->url = $user->image;
                    $galerie->save();

                }

                return $this->responseRequestSuccess($user);
            } else {
                return $this->responseRequestError('Cannot upload file');
            }
        } else {
            return $this->responseRequestError('File not found');
        }
    }

    protected function responseRequestSuccess($ret)
    {
        return response()->json(['status' => 'success', 'data' => $ret], 200)
            ->header('Access-Control-Allow-Origin', '*')
            ->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
    }

    protected function responseRequestError($message = 'Bad request', $statusCode = 200)
    {
        return response()->json(['status' => 'error', 'error' => $message], $statusCode)
            ->header('Access-Control-Allow-Origin', '*')
            ->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
    }

    //Verification

    public function verifyPassword(Request $request){

        try {
            $this->validate($request, [
                'password' => array(
                    'required',
                    'string')
            ]);

            $email = User::findOrFail(Auth::id())->email;
            $password = $request->input('password');

            if (! Auth::attempt(['email' => $email, 'password' => $password])) {
                return response()->json(['isConnect' => false], 401);
            }
        
            return response()->json(['isConnect' => true], 200);
        } catch (\Exception $e){

            return response()->json(['message' => 'Connexion Failed! Invalid email or password'], 409);

        }

    }

    //Count

    public function countUsers(){

        try {

            $nbUsers = count(DB::table("users")->where('deleted_at', NULL)->get());

            return response()->json(['nbUsers' => $nbUsers], 200);

        }catch(\Exception $e){

            return response()->json(['message' => 'Method Failed!'], 409);

        }

    }

    public function countArticles(){

        try {

            $nbArticles = count(DB::table("articles")->where('deleted_at', NULL)->get());

            return response()->json(['nbArticles' => $nbArticles], 200);

        }catch(\Exception $e){

            return response()->json(['message' => 'Method Failed!'], 409);

        }

    }

    public function countComments(){

        try {

            $nbComments = count(DB::table("comments")->where('deleted_at', NULL)->get());

            return response()->json(['nbComments' => $nbComments], 200);

        }catch (\Exception $e) {

            return response()->json(['message' => 'Method Failed!'], 409);

        }

    }

    public function countCommentsPerArticle($id){

        try {

            $nbComments = count(DB::table("comments")->where('id_article', $id)->get());

            return response()->json(['nbComments' => $nbComments], 200);

        }catch (\Exception $e) {

            return response()->json(['message' => 'Method Failed!'], 409);

        }

    }

}
