<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use ElemenX\ApiPagination\Paginatable;

class Comment extends Model{

    use Paginatable;
    use SoftDeletes;
    use HasFactory;

    public $incrementing = false;
    protected $table = 'comments';

    /**
     * 
     * The attributes that are mass assignable.
     * 
     * @var array
     * 
     */

    protected $fillable = [

        'id_user', 'id_article', 'body'

    ];

    /**
     * 
     * The attributes excluded from the model's JSON form.
     * 
     * @var array
     * 
     */

    protected $hidden = [];

}