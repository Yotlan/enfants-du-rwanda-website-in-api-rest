<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use ElemenX\ApiPagination\Paginatable;

class Article extends Model{

    use Paginatable;
    use SoftDeletes;
    use HasFactory;

    public $incrementing = false;
    protected $table = 'articles';

    /**
     
     * The attributes that are mass assignable.
     
     * @var array
     
     */

    protected $fillable = [

        'id_user', 'title', 'summary', 'body'

    ];

    /**
     
     * The attributes excluded from the model's JSON form.
     
     *
     
     * @var array
     
     */

    protected $hidden = [];

}