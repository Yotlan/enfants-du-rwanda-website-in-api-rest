<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Galerie extends Model{

    use SoftDeletes;

    public $incrementing = false;
    protected $table = 'galerie';

    /**
     * 
     * The attributes that are mass assignable.
     * 
     * @var array
     * 
     */

    protected $fillable = [

        'id', 'url'

    ];

    /**
     * 
     * The attributes excluded from the model's JSON form.
     * 
     * @var array
     * 
     */

    protected $hidden = [];

}