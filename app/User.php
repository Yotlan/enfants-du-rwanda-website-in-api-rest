<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Tymon\JWTAuth\Contracts\JWTSubject;
use ElemenX\ApiPagination\Paginatable;

class User extends Model implements AuthenticatableContract, AuthorizableContract, JWTSubject
{

    use Paginatable;
    use SoftDeletes;
    use HasFactory;

    public $incrementing = false;
    protected $table = 'users';

    /**

    * The attributes that are mass assignable.

    *

    * @var array

    */

    protected $fillable = [

        'pseudo', 'email', 'nationality', 'date_inscription', 'isAdmin', 'isMember', 'picture', 'password'

    ];

    /**

    * The attributes excluded from the model's JSON form.

    *

    * @var array

    */

    protected $hidden = [

        

    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT
     * 
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    
    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     * 
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function getAuthIdentifier()
    {
        return $this->id;
    }

    public function getAuthIdentifierName()
    {
        return 'id';
    }

    public function getAuthPassword()
    {
        return $this->password;
    }

    public function getRememberToken()
    {
        
    }

    public function getRememberTokenName()
    {
        
    }

    public function setRememberToken($value)
    {
        
    }

    public function can($abilities, $arguments = [])
    {
        
    }
}