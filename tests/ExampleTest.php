<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Faker\Factory as Faker;

use App\User;
use App\Article;
use App\Comment;

class ExampleTest extends TestCase
{

    /**
     * A basic test example.
     *
     * @return void
     */

    //Make sure you use the command php artisan db:seed before launching the command ./vendor/bin/phpunit (for test)

    //Test for User

    public function testShowAllUsersMethodWork()
    {

        $this->call('GET', 'api/users',
            ["limit" => 5,
            "offset" => 0]    
        )->assertSuccessful();

    }

    public function testShowOneUserMethodWork()
    {

        $this->call('GET', 'api/users/1')->assertSuccessful();

    }

    public function testProfileMethodWork()
    {

        $email = 'agent.test@gmail.com';
        $password = 'secret';

        $this->call('POST', 'api/auth/login', 
            ["email" => $email,
            "password" => $password]
        );

        $content = json_decode($this->response->getContent(), true);

        $this->call('GET', 'api/users/profile', [], ['Authorization' => $content['token_type'] . " " . $content['token']])->assertSuccessful();

    }

    public function testUpdateUserMethodWork()
    {

        $user = User::findOrFail(1);

        $this->actingAs($user)->call('PUT', 'api/users/1', ["pseudo" => "Agent test"])->assertSuccessful();

    }

    public function testUpdateUserMethodWorkAdmin()
    {

        $user = User::factory()->create();
        $user->isAdmin = 1;
        $user->update();

        $this->actingAs($user)->call('PUT', 'api/users/1', ["pseudo" => "Agent test", "isAdmin" => true, "isMember" => true])->assertSuccessful();
            
    }

    public function testDeleteUserMethodWork()
    {

        $user = User::factory()->create();
        $id = $user->id;

        $this->actingAs($user)->call('DELETE', 'api/users/' . $id)->assertSuccessful();

    }

    public function testDeleteUserMethodWorkAdmin()
    {

        $user = User::factory()->create();
        $user->isAdmin = 1;
        $user->update();

        $id = User::factory()->create()->id;

        $this->actingAs($user)->call('DELETE', 'api/users/' . $id)->assertSuccessful();

    }

    //Test for Auth

    public function testRegisterMethodWork()
    {

        $faker = Faker::create();

        $this->call('POST', 'api/auth/register', 
            ["pseudo" => $faker->name(16), 
            "email" => $faker->safeEmail(), 
            "password" => '$S3cr3t5', 
            "nationality" => $faker->countryCode()]
        )->assertSuccessful();

    }

    public function testLoginMethodWork()
    {

        $email = 'agent.test@gmail.com';
        $password = 'secret';

        $this->call('POST', 'api/auth/login', 
            ["email" => $email,
            "password" => $password]
        )->assertSuccessful();

    }

    //Test for Article

    public function testShowAllArticlesMethodWork()
    {

        $this->call('GET', 'api/articles',
            ["limit" => 5,
            "offset" => 0]    
        )->assertSuccessful();

    }

    public function testShowOneArticleMethodWork()
    {

        $this->call('GET', 'api/articles/1')->assertSuccessful();

    }

    public function testShowAllCommentsOfArticleMethodWork(){

        $this->call('GET', 'api/articles/1',
            ["limit" => 5,
            "offset" => 0]    
        )->assertSuccessful();

    }

    public function testCreateArticleMethodWork()
    {

        $faker = Faker::create();
    
        $email = 'agent.test@gmail.com';
        $password = 'secret';

        $this->call('POST', 'api/auth/login', 
            ["email" => $email,
            "password" => $password]
        );

        $content = json_decode($this->response->getContent(), true);

        $this->call('POST', 'api/articles', 
            ["title" => 'Article test',
            "summary" => $faker->text(100),
            "body" => $faker->text()], 
            ['Authorization' => $content['token_type'] . " " . $content['token']]
        )->assertSuccessful();

    }

    public function testUpdateArticleMethodWork()
    {

        $email = 'agent.test@gmail.com';
        $password = 'secret';

        $this->call('POST', 'api/auth/login', 
            ["email" => $email,
            "password" => $password]
        );

        $content = json_decode($this->response->getContent(), true);

        $this->call('PUT', 'api/articles/1', ["title" => "Article test"],
        ['Authorization' => $content['token_type'] . " " . $content['token']]
        )->assertSuccessful();

    }

    public function testDeleteArticleMethodWork()
    {

        $email = 'agent.test@gmail.com';
        $password = 'secret';

        $this->call('POST', 'api/auth/login', 
            ["email" => $email,
            "password" => $password]
        );

        $content = json_decode($this->response->getContent(), true);

        $article = Article::factory()->create();
        $id = $article->id;

        $this->call('DELETE', 'api/articles/' . $id,
        ['Authorization' => $content['token_type'] . " " . $content['token']]
        )->assertSuccessful();

    }

    public function testDeleteArticleMethodWorkAdmin()
    {

        $user = User::factory()->create();
        $user->isAdmin = 1;
        $user->update();

        $email = $user->email;
        $password = 'secret';

        $this->call('POST', 'api/auth/login', 
            ["email" => $email,
            "password" => $password]
        );

        $content = json_decode($this->response->getContent(), true);

        $article = Article::factory()->create();
        $id = $article->id;

        $this->call('DELETE', 'api/articles/' . $id,
        ['Authorization' => $content['token_type'] . " " . $content['token']]
        )->assertSuccessful();

    }

    //Test for Comment

    public function testShowAllCommentsMethodWork()
    {

        $this->call('GET', 'api/comments',
            ["limit" => 5,
            "offset" => 0]    
        )->assertSuccessful();

    }

    public function testShowOneCommentMethodWork()
    {

        $this->call('GET', 'api/comments/1')->assertSuccessful();

    }

    public function testCreateCommentMethodWork()
    {

        $faker = Faker::create();
    
        $email = 'agent.test@gmail.com';
        $password = 'secret';

        $this->call('POST', 'api/auth/login', 
            ["email" => $email,
            "password" => $password]
        );

        $content = json_decode($this->response->getContent(), true);

        $this->call('POST', 'api/comments', 
            ["id_article" => 1,
            "body" => 'Commentaire test'],
            ['Authorization' => $content['token_type'] . " " . $content['token']]
        )->assertSuccessful();

    }

    public function testUpdateCommentMethodWork()
    {

        $email = 'agent.test@gmail.com';
        $password = 'secret';

        $this->call('POST', 'api/auth/login', 
            ["email" => $email,
            "password" => $password]
        );

        $content = json_decode($this->response->getContent(), true);

        $this->call('PUT', 'api/comments/1', ["body" => "Commentaire test"],
        ['Authorization' => $content['token_type'] . " " . $content['token']]
        )->assertSuccessful();

    }

    public function testDeleteCommentMethodWork()
    {

        $email = 'agent.test@gmail.com';
        $password = 'secret';

        $this->call('POST', 'api/auth/login', 
            ["email" => $email,
            "password" => $password]
        );

        $content = json_decode($this->response->getContent(), true);

        $comment = Comment::factory()->create();
        $id = $comment->id;

        $this->call('DELETE', 'api/comments/' . $id,
        ['Authorization' => $content['token_type'] . " " . $content['token']]
        )->assertSuccessful();

    }

    public function testDeleteCommentMethodWorkAdmin()
    {

        $user = User::factory()->create();
        $user->isAdmin = 1;
        $user->update();

        $email = $user->email;
        $password = 'secret';

        $this->call('POST', 'api/auth/login', 
            ["email" => $email,
            "password" => $password]
        );

        $content = json_decode($this->response->getContent(), true);

        $comment = Comment::factory()->create();
        $id = $comment->id;

        $this->call('DELETE', 'api/comments/' . $id,
        ['Authorization' => $content['token_type'] . " " . $content['token']]
        )->assertSuccessful();

    }

    //Test for verification

    public function testVerifyPassword()
    {

        $email = 'agent.test@gmail.com';
        $password = 'secret';

        $this->call('POST', 'api/auth/login', 
            ["email" => $email,
            "password" => $password]
        );

        $content = json_decode($this->response->getContent(), true);

        $this->call('POST', 'api/users/verify', ["password" => $password],
            ['Authorization' => $content['token_type'] . " " . $content['token']]
        )->assertSuccessful();

    }

    //Test for number of something

    public function testNumberOfUsers()
    {

        $this->call('GET', 'api/users/count')->assertSuccessful();

    }

    public function testNumberOfArticles()
    {

        $this->call('GET', 'api/articles/count')->assertSuccessful();

    }

    public function testNumberOfComments()
    {

        $this->call('GET', 'api/comments/count')->assertSuccessful();

    }

    public function testNumberOfCommentsPerArticles()
    {

        $this->call('GET', 'api/articles/comments/' . 1 . '/count')->assertSuccessful();

    }

}
