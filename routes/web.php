<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('/key', function() {
    return \Illuminate\Support\Str::random(32);
});

$router->group(['prefix' => 'api'], function () use ($router) {

    //router for User & his Authentification

    $router->get('users/count', ['uses' => 'Controller@countUsers']);

    $router->get('users/profile', ['uses' => 'UserController@profile']);

    $router->get('users/{id}', ['uses' => 'UserController@showOneUser']);

    $router->get('users', ['uses' => 'UserController@showAllUsers']);

    $router->post('auth/register', ['uses' => 'AuthController@register']);

    $router->post('auth/login', ['uses' => 'AuthController@login']);

    $router->post('users/{id}/picture', ['uses' => 'UserController@uploadImageIntoUser']);

    $router->post('users/verify', ['uses' => 'Controller@verifyPassword']);

    $router->put('users/{id}', ['uses' => 'UserController@update']);

    $router->delete('users/{id}', ['uses' => 'UserController@delete']);

    //router for Article

    $router->get('articles/count', ['uses' => 'Controller@countArticles']);

    $router->get('articles/comments/{id}/count', ['uses' => 'Controller@countCommentsPerArticle']);

    $router->get('articles/comments/{id}', ['uses' => 'ArticleController@showAllCommentsOfOneArticle']);

    $router->get('articles/{id}', ['uses' => 'ArticleController@showOneArticle']);

    $router->get('articles', ['uses' => 'ArticleController@showAllArticles']);

    $router->post('articles', ['uses' => 'ArticleController@create']);

    $router->post('articles/{id}/picture', ['uses' => 'ArticleController@updatePicture']);

    $router->put('articles/{id}', ['uses' => 'ArticleController@update']);

    $router->delete('articles/{id}', ['uses' => 'ArticleController@delete']);

    //router for Comment

    $router->get('comments/count', ['uses' => 'Controller@countComments']);

    $router->get('comments/{id}', ['uses' => 'CommentController@showOneComment']);

    $router->get('comments', ['uses' => 'CommentController@showAllComments']);

    $router->post('comments', ['uses' => 'CommentController@create']);

    $router->put('comments/{id}', ['uses' => 'CommentController@update']);

    $router->delete('comments/{id}', ['uses' => 'CommentController@delete']);

    //router for Galerie

    $router->get('galerie/{id}', ['uses' => 'GalerieController@showOneImage']);
    
    $router->get('galerie', ['uses' => 'GalerieController@showAllImages']);

    $router->post('galerie', ['uses' => 'GalerieController@uploadImageIntoGalerie']);

    $router->delete('galerie/{id}', ['uses' => 'GalerieController@deleteImage']);

});
