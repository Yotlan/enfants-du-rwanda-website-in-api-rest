<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call('UsersTableSeeder');

        $faker = Faker::create();
        $originalUser = 1;
        $originalArticle = 1;

        DB::table('users')->insert([
            'id' => $originalUser,
            'pseudo' => 'Agent test',
            'email' => 'agent.test@gmail.com',
            'password' => app('hash')->make('secret'),
            'nationality' => $faker->countryCode,
            'isAdmin' => true,
            'isMember' => true,
            'picture' => 'default_user.png'
        ]);

        for ($i=0; $i<11; $i++) {

            DB::table('users')->insert([
                'id' => $faker->uuid,
                'pseudo' => $faker->name,
                'email' => $faker->safeEmail,
                'password' => app('hash')->make('secret'),
                'nationality' => $faker->countryCode,
                'picture' => 'default_user.png'
            ]);

        }

        DB::table('articles')->insert([
            'id' => $originalArticle,
            'id_user' => $originalUser,
            'title' => 'Article test',
            'summary' => $faker->text(100),
            'body' => $faker->text,
            'picture' => 'default_article.png'
        ]);

        DB::table('comments')->insert([
            'id' => 1,
            'id_user' => $originalUser,
            'id_article' => $originalArticle,
            'body' => 'Commentaire test'
        ]);

    }
}
