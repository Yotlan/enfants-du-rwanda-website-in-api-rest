<?php

namespace Database\Factories;

use App\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'id' => $this->faker->uuid(),
            'pseudo' => $this->faker->name(),
            'email' => $this->faker->unique()->safeEmail(),
            'password' => app('hash')->make('secret'),
            'nationality' => $this->faker->countryCode(),
            'picture' => 'default_user.png'
        ];
    }
}
