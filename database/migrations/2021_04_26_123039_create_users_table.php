<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::enableForeignKeyConstraints();
        Schema::create('users', function (Blueprint $table) {
            $table->uuid('id')
                ->unique()
                ->primary();
            $table->string('pseudo')
                ->unique()
                ->notNullable();
            $table->string('email')
                ->unique()
                ->notNullable();
            $table->string('password')
                ->notNullable();
            $table->string('nationality')
                ->notNullable();
            $table->boolean('isAdmin')
                ->default(false);
            $table->boolean('isMember')
                ->default(false);
            $table->text('picture'); //default value : default_user.png
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
