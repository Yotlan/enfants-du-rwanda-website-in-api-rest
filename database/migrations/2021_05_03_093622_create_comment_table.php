<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::enableForeignKeyConstraints();
        Schema::create('comments', function (Blueprint $table) {
            $table->uuid('id')
                ->unique()
                ->primary();
            $table->uuid('id_user')
                ->notNullable();
            $table->foreign('id_user')
                ->references('id')
                ->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->uuid('id_article')
                ->notNullable();
            $table->foreign('id_article')
                ->references('id')
                ->on('articles')
                ->notNullable()
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->longText('body')
                ->notNullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
